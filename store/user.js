export default{
  namespaced:true,
  state:()=>({
    // 3. 读取本地的收货地址数据，初始化 address 对象
    address: JSON.parse(uni.getStorageSync('address')||'{}'),
    token: uni.getStorageSync('token')||'',
    userinfo:JSON.parse(uni.getStorageSync('userinfo')||'{}'),
    redirectInfo:[],
    user: JSON.parse(uni.getStorageSync('user')||'{}'),
  }), 
  mutations:{ 
    updateAddress(state,address){
      state.address = address
      // 2. 通过 this.commit() 方法，调用 m_user 模块下的 saveAddressToStorage 方法将 address 对象持久化存储到本地
      this.commit('m_user/saveAddressToStorage')
    },
    updateUserInfo(state,userinfo){
      state.userinfo = userinfo
      this.commit('m_user/saveUserinfoToStorage')
    },
    updateToken(state,token){
      state.token = token
      this.commit('m_user/saveTokenToStorage')
    },
    updateRedirectInfo(state,info){
      console.log(info);
      state.redirectInfo = info
      // this.commit('m_user/save')
    },
    updataUser(state,a){
      state.user = a
      this.commit('m_user/saveUserToStorage')
    },
    // 1. 定义将 address 持久化存储到本地 mutations 方法
    saveAddressToStorage(state){
      uni.setStorageSync('address',JSON.stringify(state.address))
    },
    saveUserinfoToStorage(state){
      uni.setStorageSync('userinfo',JSON.stringify(state.userinfo))
    },
    saveTokenToStorage(state){
      uni.setStorageSync('token',state.token)
    }
    ,
    saveUserToStorage(state){
      uni.setStorageSync('user',JSON.stringify(state.user))
    }
    
  },
  //数据包装器  地址拼接
  getters:{
    addstr(state){
      if(!state.address.cityName) return ''
      return state.address.provinceName + state.address.cityName + state.address.countyName + state.address.detailInfo
    }
  }
}