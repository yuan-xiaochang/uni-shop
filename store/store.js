//1.导入vue和vuex
import Vue from 'vue'
import Vuex from 'vuex'
import cart from './cart'
import user from './user'

//2.将vuex安装为vue插件
Vue.use(Vuex)

//3.创建store的实例对象
const store = new Vuex.Store({
  //挂载store模块
  modules:{
    // 2. 挂载购物车的 vuex 模块，模块内成员的访问路径被调整为 m_cart，例如：
        //    购物车模块中 cart 数组的访问路径是 m_cart/cart
        m_cart:cart,
        m_user:user
  }
})

//向外共享store的实例对象
export default store