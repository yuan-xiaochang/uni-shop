
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import {$http} from '@escook/request-miniprogram'
import store from 'store/store.js'

Vue.config.productionTip = false
uni.$http = $http
// 配置请求路径
$http.baseUrl = 'https://www.uinav.com'
  
uni.$showMsg = function(title='数据加载失败！',duration =1500){
  uni.showToast({
    title,
    duration,
    icon:'none'
  })
}
//请求开始之前做一些事情
$http.beforeRequest = function(options){
  uni.showLoading({
    title:'数据加载中...',
  })
  
  //判断请求是否是有权限的一些事
  if(options.url.indexOf('/my/')!=-1){
    //为请求头添加身份认证字段   
    options.header={
      Authorization:store.state.m_user.token
    }
  }
}
//请求完成之后做一些事情
$http.afterRequest = function(){
  uni.hideLoading()
}
App.mpType = 'app'

const app = new Vue({
    ...App,
    store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif